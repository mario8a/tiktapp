import React from 'react';
import { Outlet, 
         Route, 
         Routes, 
         useNavigate, 
         Link, 
         Navigate
} from 'react-router-dom';
import {  } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logOut } from '../store/user';
//Components
import { SignIn } from '../users/SignIn';
import { Videos } from '../videos/Videos';
import { VideosForm } from '../videos/VideosForm';
import { VideoShow } from '../videos/VideoShow';
import { Profile } from '../users/Profile';
import { Home } from '../Home';
import { SignUp } from '../users/SignUp';

export const AppRoutes = (props) => {

   let user = useSelector(state => state.user.user);

   let NotImplemented = () => {
      return (
        <>
          <Link to="/videos">Ir a videos</Link>
          <h1>Esta pagina aun no esta lista</h1>
        </>
      )
    }
    
    let Error404 = () => {
      return (
        <>
          <Link to="/">Regresar al inicio</Link>
          <h1>Pagina no disponible</h1>
        </>
      )
    }

   return (
      <Routes>
         <Route path="/" element={<Home />} />

         <Route path="/usuarios" element={ user ? <Navigate to="/videos"/> : <Outlet />} >
           <Route path="registro" element={<SignUp />} />
           <Route path="login" element={<SignIn />} />
         </Route>

         <Route path="/" element={ user ? <Outlet /> : <Navigate to="/usuarios/login"/>}>

            <Route path="usuarios/miperfil" element={<Profile />} />
            <Route path="usuarios/:id/videos" element={<NotImplemented />} />


            <Route path="/videos">
               <Route path="/" element={<Videos />} />
               <Route path=":id" element={<VideoShow />} />
               <Route path="nuevo" element={<VideosForm />} />
            </Route>
         </Route>

         <Route path="*" element={<Error404 />} ></Route>

      </Routes>
   )
}
