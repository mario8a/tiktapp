import React from 'react'
import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { AppInput, Fieldset } from '../components/AppInput';
import { createVideo } from '../store/videos';
import { AppButton, CenteredContainer, SmallContainer as SmallContainerTemplate } from '../theme';

export const VideosForm = () => {
   let {register, handleSubmit } = useForm();
   let dispatch = useDispatch();

   let onSubmit = async(video) => {
      let formData = new FormData();
      

      formData.append('title', video.title);
      formData.append('video', video.video[0]);

      // console.log(formData);
      dispatch(
         createVideo(formData)
      )
   }

   let SmallContainer = styled(SmallContainerTemplate)`
      text-align: center;
   `;

   return (
      <CenteredContainer>
         <SmallContainer>
            <form onSubmit={handleSubmit(onSubmit)}>
               <AppInput type="text" name="title" register={register} label="Titulo"/>
               <Fieldset>
                  <label>Archivo del video</label>
                  <input type="file" name="video" ref={register} />
               </Fieldset>
               <AppButton type="submit" small>Subir video</AppButton>
            </form>
         </SmallContainer>
      </CenteredContainer>
   )
}
