import React from 'react'
import { ClearButton, SvgButton } from '../theme'

export const ShareButton = ({video}) => {


   return (
      <ClearButton>
         <SvgButton src="/share.svg"></SvgButton>
      </ClearButton>
   )
}
