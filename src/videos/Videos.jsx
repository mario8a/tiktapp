import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
//store
import { loadVideos } from '../store/videos';
import { SmallContainer } from '../theme';
import { Video } from './Video';
import { VideosList } from './VideosList';

export const Videos = () => {
   let videosState = useSelector(state => state.videos);
   let dispatch = useDispatch();

   // const [loading, setLoading] = useState(false);

   // let loadNextPage = async () => {
   //    setLoading(true);
   //    await dispatch( loadVideos() );
   //    setLoading(false);
   // }

   useEffect(()=>{
      dispatch(
        loadVideos()
      )
    },[]);


   return (
      <div>
         <SmallContainer>
            {/* <VideosList loadNextPage={loadNextPage} videosState={videosState} loading={loading}>

            </VideosList> */}
            {
               videosState.data.videos.map((video, index) => (
                  <Video key={index} index={index} video={video}></Video>
               ))
            }
         </SmallContainer>
      </div>
   )
}
