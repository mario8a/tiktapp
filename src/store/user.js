import {
   createAsyncThunk,
   createSlice
} from '@reduxjs/toolkit';

import Axios from 'axios';
import apiConfig from '../config/api';
//*los thunks modifican el state por meido de operaciones asincronas
//*Registro de usuarios
export const signUp = createAsyncThunk('user/signUp', async({credentials}) => {
   // Async operation
   let response = await Axios.post(`${apiConfig.domain}/users`, {
      user: credentials
   });
   console.log(response);
   return response.data.user;
});

//*Inicio de sesion
export const signIn = createAsyncThunk('user/signUp', async({credentials}) => {
   // Async operation
   let response = await Axios.post(`${apiConfig.domain}/users/signin`, {
      user: credentials
   });
   // console.log(response);
   return response.data.user;
});

//definir las actions que val a modificar el state
let userSlice = createSlice({
   name: 'user',
   initialState: {
      user: null,
      status: ''
   },
   reducers: {
      logOut: (state) => {
         state.user = null;
      }
   },
   extraReducers: {
      [signUp.pending]: (state, action) => {
         state.status = 'Loading';
      },
      [signUp.fulfilled]: (state, action) => {
         state.user = action.payload;
         state.status = 'Success';
      },
      [signUp.rejected]: (state, action) => {
         state.status = 'Failed';
      },
      [signIn.pending]: (state, action) => {
         state.status = 'Loading';
      },
      [signIn.fulfilled]: (state, action) => {
         state.user = action.payload;
         state.status = 'Success';
      },
      [signIn.rejected]: (state, action) => {
         state.status = 'Failed';
      },
   }
});
// Actions creators -> de createSlice
export const { logOut } = userSlice.actions;

export default userSlice.reducer;