import React from 'react'
import { useDispatch } from 'react-redux'
import { ClearButton } from '../theme';

import { logOut } from '../store/user';

export const LogoutButton = (props) => {

   let dispatch = useDispatch();

   let logOutuser = () => {
      dispatch(
         logOut()
      )
   }

   return (
      <ClearButton className={props.className} onClick={logOutuser}>
         Cerrar sesión
      </ClearButton>
   )
}
