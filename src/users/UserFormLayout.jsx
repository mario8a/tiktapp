import React from 'react'
import styled from 'styled-components'
import { CenteredContainer, SmallContainer  as SmallContainerTemplate, Title} from '../theme'

let SmallContainer = styled(SmallContainerTemplate)`
   text-align: center;
`;

let Header = styled.header`
   text-align: center;
   margin-bottom: ${({ theme }) => theme.dims.margin.normal};
`;

export const UserFormLayout = (props) => {
   return (
      <CenteredContainer>
         <SmallContainer>
            <Header>
               <img src="/logo.svg" alt="Logo de la aplicacion" height="120"/>
               <div>
                  <Title>TikTak</Title>
               </div>
            </Header>
            {props.children}
         </SmallContainer>
      </CenteredContainer>
   )
}
