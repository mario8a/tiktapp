import {
  BrowserRouter as Router,
} from 'react-router-dom';

import { Provider } from 'react-redux'
import { persistor, store } from './store';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider } from 'styled-components';
//Components
import { Layout } from './components/Layout';
import { AppRoutes } from './routes/AppRoutes';
//theme
import theme, { GlobalStyles } from './theme';




function App() {
  return (
    <Router>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ThemeProvider theme={theme}>
          <GlobalStyles/>
            <Layout>
            {/* Todo componente dentro de Provider tiene acceso al store */}
              <AppRoutes/>
            </Layout>
          </ThemeProvider>
        </PersistGate>
      </Provider>
    </Router>
  );
}

export default App;
