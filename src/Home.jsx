import React from 'react';
import { Link } from 'react-router-dom';
import styled, {css} from 'styled-components';

//*Reutilizar bloques de css
let linkStyle = css`
   font-size: 0.5em;
`;

//?Tagged templates
let Example = styled.div`
   height: 200px;
   padding: 50px;
/* Nesting o anidado */
   & a {
      color: purple;
   }

   &.important {
      background-color: yellow;
   }
`;

let Button = styled.button`
   border: solid 1px blue;
   background-color: ${({primary}) => primary ? 'red' : 'transparend'}; 
   outline: 0;
   font-size: 1em;
   box-shadow: 2px 2px 2px rgba(0,0,0,0.6);
   //Reutiliznadop el estilo
   ${linkStyle}
`;
// Extendiendo estilos a componentes styled
let PrimaryButton = styled(Button)`
   background-color: blue;
   border: 0;
   color: white;
   ${linkStyle}
`;

let AppLink = styled(Link)`
   text-decoration: underline;
   font-weight: bold;
   text-transform: uppercase;
`;

let AppInput = styled.input.attrs((props => {
    return {
       type: props.type ? props.type : 'text'
     }
}))`
   border: solid 1px red;
   .important & {
      background-color: pink;
   }
`;

export const Home = () => {
   return (
      <Example>
         {/* <Button primary>Ejemplo</Button>
         <PrimaryButton>Botom primario</PrimaryButton>
         <AppLink to="/videos">Ir a videos</AppLink>
         <AppInput type="text" placeholder="Escribe tu nombre"></AppInput> */}
         <PrimaryButton>Enivar</PrimaryButton>
         <Button>Cancelar</Button>
         <AppInput></AppInput>
      </Example>
   )
} 
